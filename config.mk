# dmenu version
VERSION = 5.0

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

PKG_CONFIG = pkg-config

# Xinerama, comment if you don't want it
#XINERAMALIBS  = -lXinerama
#XINERAMAFLAGS = -DXINERAMA

# freetype
FREETYPELIBS = -lfontconfig -lXft
FREETYPEINC = /usr/include/freetype2
# OpenBSD (uncomment)
#FREETYPEINC = $(X11INC)/freetype2

# includes and libs
INCS = -I$(X11INC) -I$(FREETYPEINC)
LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS) -lXrender -lm -lXft

# flags
RELEASE = -Os -march=native -mtune=native -flto -fno-semantic-interposition -fipa-pta -fdevirtualize-at-ltrans -fuse-linker-plugin -fgraphite-identity -floop-nest-optimize -fipa-pta -fno-common -fno-math-errno -fno-trapping-math -fdevirtualize-at-ltrans -fuse-linker-plugin -fomit-frame-pointer -ffloat-store -fexcess-precision=fast -ffast-math -fno-rounding-math -fno-signaling-nans -fcx-limited-range -fno-math-errno -funsafe-math-optimizations -fassociative-math -freciprocal-math -ffinite-math-only -fno-signed-zeros -fno-trapping-math -frounding-math -fsingle-precision-constant -fcx-fortran-rules
DEBUG = -g
BUILDTYPE = $(RELEASE)
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L -DVERSION=\"$(VERSION)\" $(XINERAMAFLAGS)
CFLAGS   = -std=c99 -pedantic -Wall $(BUILDTYPE) $(WARN) $(OPTIMIZE) $(INCS) $(CPPFLAGS)
LDFLAGS  = $(BUILDTYPE) $(LIBS)

# compiler and linker
CC = cc
