/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;		/* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;        /* -c option; centers dmenu on screen */
static int min_width = 500;     /* minimum width when centered */
static int colorprompt = 1;	/* -p  option; if 1, prompt uses SchemeSel, otherwise SchemeNorm */
static int fuzzy = 1;		/* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static char passmask = '*';	/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"InconsolataLGC Nerd Font:bold:pixelsize=14:antialias=true:autohint=true",
	"Noto Color Emoji:12",
};

static const char *prompt =
	NULL; /* -p  option; prompt to the left of input field */

#define fuzmatch "#f7768e"
#define textcolor "#7aa2f7"
#define bg "#24283b"
#define fg "#eeeeee"

static const char *colors[SchemeLast][2] = {
	/*                        fg           bg*/
	[SchemeNorm]          = { textcolor, bg },
	[SchemeSel]           = { bg,        textcolor },
	[SchemeOut]           = { textcolor, fg },
	[SchemeSelHighlight]  = { fuzmatch,  textcolor },
	[SchemeNormHighlight] = { fuzmatch,  bg },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 3;

