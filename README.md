# Doris's dmenu

### Extra stuff added
 * Compilation and optimization
   * No warning even with `-Wall -Wextra -Werror`.
   * Compiled with `-Os` and LTO.
   * `strip --strip-all dmenu`.
 * Highlight matched characters.
 * Mouse support.
 * Case insensitive search.
 * Added `-P` option to subsitute input character with `passmask` variable.

---

### Dependency
* Hack Nerd Font Mono: the font used by `dmenu`.

---

### Patches applied:
 * [caseinsensitive](https://tools.suckless.org/dmenu/patches/case-insensitive/dmenu-caseinsensitive-5.0.diff)
 * [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff)
 * [highlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/dmenu-fuzzyhighlight-4.9.diff)
 * [listfullwidth](https://tools.suckless.org/dmenu/patches/listfullwidth/dmenu-listfullwidth-5.0.diff)
 * [mousesupporthoverbgcol](https://tools.suckless.org/dmenu/patches/mouse-support/dmenu-mousesupporthoverbgcol-20210123-1a13d04.diff)
 * [numbers](https://tools.suckless.org/dmenu/patches/numbers/dmenu-numbers-4.9.diff)
 * [password](https://tools.suckless.org/dmenu/patches/password/dmenu-password-5.0.diff)
